package com.aurbano.app;

import com.aurbano.utilidades.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by adriu on 20/02/2016.
 */

public class ConexionCliente implements Runnable {

    private String nick;
    private String contrasena;
    private Servidor servidor;
    private Socket socket;

    private BufferedReader entrada;
    private PrintWriter salida;

    private boolean conectado;
    private boolean ipRestringida;

    public ConexionCliente(Servidor servidor, Socket socket) throws IOException {
        this.servidor = servidor;
        this.socket = socket;

        entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        salida = new PrintWriter(socket.getOutputStream(), true);
        conectado = true;
    }

    public void salida(String mensaje) {
        salida.println(mensaje);
    }

    public void notificarConexion(String mensaje) {
        String[] partes = mensaje.split("-");
        partes = partes[0].split(":");
        String nombre = partes[1].replace(" ", "");

        if (nombre.equalsIgnoreCase(nick)) {
            salida.println("[info]: Te has  conectado como " + nick);
            return;
        }

        salida.println(mensaje.replace("-", " "));
    }

    public void notificarDesconexion(String mensaje) {
        String[] partes = mensaje.split("-");
        partes = partes[0].split(":");
        String nombre = partes[1].replace(" ", "");

        if (nombre.equalsIgnoreCase(nick)) {
            salida.println("[info]: Te has desconectado");
            return;
        }

        salida.println(mensaje.replace("-", " "));
    }

    public String getIP() {
        return socket.getInetAddress().getHostAddress();
    }

    public String getNick() {
        return nick;
    }

    public void setIpRestringida(boolean ipRestringida) {
        this.ipRestringida = ipRestringida;
    }

    @Override
    public void run() {
        try {
            nick = entrada.readLine();
            contrasena = entrada.readLine();

            if (ipRestringida) {
                salida.println("[ipRestringida]:");
                return;
            }

            if (nick.equals("/debug"))
                conectado = false;

            if (nick.startsWith("[registro]:"))
                procesarRegistro();

            if (!conectado) {
                servidor.getConexiones().remove(this);
                return;
            }

            if (servidor.getBaseDatos().existeUsuario(nick) && !servidor.getBaseDatos().loginCorrecto(nick, contrasena)) {
                salida.println("[loginFallido]: Login incorrecto");
                conectado = false;
            }

            if (servidor.nombreUsuarioCogido(nick)) {
                conectado = false;
                salida.println("[loginFallido]: Ya hay un usuario con ese nick");
            }

            if (!conectado) {
                servidor.getConexiones().remove(this);
                return;
            }

            servidor.getConexiones().add(this);
            servidor.notificarUsuarioConectado("[info]: " + nick + "- se ha conectado");
            Utils.escribirLog(nick + " se ha conectado");

            while (conectado)
                procesarPeticion();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void procesarRegistro() {
        nick = nick.replace("[registro]:", "");
        conectado = servidor.getBaseDatos().altaUsuario(nick, contrasena);

        if (!conectado)
            salida.println("[registro]:" + "Ya existe un usuario con ese nombre");
    }

    private void procesarPeticion() throws IOException {
        String peticion = entrada.readLine();
        System.out.println(peticion);

        switch (peticion) {
            case "/conectados":
                salida.println(servidor.getUsuariosConectados());
                break;
            case "/nuevaDesconexion":
                conectado = false;
                servidor.notificarUsuarioDesconectado("[info]: " + nick + "- se ha desconectado");
                servidor.getConexiones().remove(this);
                Utils.escribirLog(nick + " se ha desconectado");
                break;
            default:
                servidor.notificarParaTodos(peticion);
        }

        if (peticion.startsWith("@El usuario "))
            Utils.escribirLog(peticion);
    }

}

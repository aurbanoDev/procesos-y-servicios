package com.aurbano.app;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Created by adriu on 20/02/2016.
 */
public class Servidor {

    public static final int PUERTO = 5555;

    private ArrayList<ConexionCliente> conexiones;
    private BaseDatos baseDatos;
    private ServerSocket serverSocket;
    private ScheduledExecutorService executor;

    private boolean enFuncionamiento;


    public Servidor() {
        executor = Executors.newScheduledThreadPool(10);
        conexiones = new ArrayList<>();
        baseDatos = new BaseDatos();
    }

    public void arrancar() throws IOException {
        serverSocket = new ServerSocket(PUERTO);
        enFuncionamiento = true;
        baseDatos.conectar();
    }

    public Socket aceptarConexion() throws IOException {
        Socket socket = serverSocket.accept();

        return socket;
    }

    public boolean existeIP(Socket socket) {
        for (ConexionCliente conexionCliente : conexiones) {
            if (conexionCliente.getIP().equals(socket.getInetAddress().getHostAddress()))
                return true;
        }

        return false;
    }

    public void lanzar(ConexionCliente conexion) {
        executor.submit(conexion);
    }

    public boolean isEnFuncionamiento() {
        return enFuncionamiento;
    }

    public void visualizarListaIPs() {
        System.out.println("Lista de IPS");

        for (ConexionCliente conexionCliente : conexiones)
            System.out.println(conexionCliente.getNick() + "  " + conexionCliente.getIP());

    }

    public List<String> getUsuariosConectados() {
        List lista = new ArrayList<>();

        for (ConexionCliente conexionCliente : conexiones)
            lista.add(conexionCliente.getNick());

        return lista;
    }

    public ArrayList<ConexionCliente> getConexiones() {
        return conexiones;
    }

    public void notificarUsuarioConectado(String mensaje) {
        for (ConexionCliente conexionCliente : conexiones)
            conexionCliente.notificarConexion(mensaje);
    }

    public void notificarUsuarioDesconectado(String mensaje) {
        for (ConexionCliente conexionCliente : conexiones)
            conexionCliente.notificarDesconexion(mensaje);
    }

    public void notificarParaTodos(String mensaje) {
        for (ConexionCliente conexionCliente : conexiones)
            conexionCliente.salida(mensaje);
    }

    public boolean nombreUsuarioCogido(String usuario) {
        System.out.println(getUsuariosConectados());
        for (String nombre : getUsuariosConectados()) {
            if (nombre.equalsIgnoreCase(usuario))
                return true;
        }

        return false;
    }

    public BaseDatos getBaseDatos() {
        return baseDatos;
    }
}

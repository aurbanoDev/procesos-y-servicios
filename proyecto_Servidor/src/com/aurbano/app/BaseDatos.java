package com.aurbano.app;


import com.aurbano.utilidades.Constantes;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;

/**
 * Created by adriu on 20/02/2016.
 */
public class BaseDatos {

    private Connection conexion;

    public BaseDatos() {
        File carpetaLogs = new File(Constantes.PATH_LOGS);

        if (!carpetaLogs.exists())
            carpetaLogs.mkdir();
    }

    public void conectar() {
        try {
            Class.forName("org.sqlite.JDBC");
            conexion = DriverManager.getConnection("jdbc:sqlite:" + Constantes.PATH_BD);
        }catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<String> getUsuarios() {
        ArrayList<String> lista = new ArrayList<>();
        String sql = "SELECT * FROM usuarios";

        PreparedStatement statement= null;
        ResultSet resultado = null;
        try {
            statement = conexion.prepareStatement(sql);

            resultado = statement.executeQuery();
            while (resultado.next())
                lista.add(resultado.getString(1));

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return lista;
    }

    public boolean existeUsuario(String usuario) {
        String sql = "SELECT * FROM usuarios where nombre = ?";

        PreparedStatement statement= null;
        ResultSet resultado = null;
        try {
            statement = conexion.prepareStatement(sql);
            statement.setString(1, usuario);

            resultado = statement.executeQuery();
            if (!resultado.next())
                return false;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public boolean loginCorrecto(String usuario, String contrasena) {
        String sql = "SELECT * FROM usuarios where nombre = ? AND contrasena = ?";

        PreparedStatement statement= null;
        ResultSet resultado = null;
        try {
            statement = conexion.prepareStatement(sql);
            statement.setString(1, usuario);
            statement.setString(2, contrasena);

            resultado = statement.executeQuery();
            if (!resultado.next())
                return false;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return true;
    }

    public boolean altaUsuario(String usuario, String contrasena) {
        if (existeUsuario(usuario))
            return false;

        String sql = "INSERT INTO usuarios(nombre,  contrasena) VALUES(? ,?)";

        PreparedStatement statement = null;
        try {
            statement = conexion.prepareStatement(sql);
            statement.setString(1, usuario);
            statement.setString(2, contrasena);
            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

}

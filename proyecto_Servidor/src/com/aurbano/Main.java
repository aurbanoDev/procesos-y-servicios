package com.aurbano;

import com.aurbano.app.ConexionCliente;
import com.aurbano.app.Servidor;

import java.io.IOException;
import java.net.Socket;

/**
 * Created by adriu on 20/02/2016.
 */
public class Main {

    public static void main(String[] args) {
        Servidor servidor = new Servidor();

        try {
            servidor.arrancar();

            while (servidor.isEnFuncionamiento()) {
                Socket socket = servidor.aceptarConexion();

                ConexionCliente conexionCliente = new ConexionCliente(servidor, socket);
                if (servidor.existeIP(socket))
                    conexionCliente.setIpRestringida(true);

                servidor.lanzar(conexionCliente);

                System.out.println(servidor.getUsuariosConectados());
                System.out.println(servidor.getBaseDatos().getUsuarios());
                servidor.visualizarListaIPs();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

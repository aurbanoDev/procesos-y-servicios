package com.aurbano.utilidades;

import java.io.File;

/**
 * Created by adriu on 23/02/2016.
 */
public class Constantes {

    public static final String PATH_BD = System.getProperty("user.home") + File.separator + "chat.db";
    public static final String PATH_LOGS = System.getProperty("user.home") + File.separator  + "chatLogs" + File.separator;

}

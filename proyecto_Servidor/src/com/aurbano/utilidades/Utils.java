package com.aurbano.utilidades;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.GregorianCalendar;

/**
 * Created by adriu on 22/02/2016.
 */
public class Utils {

    public static void escribirLog(String linea) {
        GregorianCalendar calendario = new GregorianCalendar();
        String log = "[" + calendario.getTime() + "] " + linea;

        try (BufferedWriter writer = new BufferedWriter(new FileWriter("serverLog.txt", true))) {
                writer.write(log);
                writer.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

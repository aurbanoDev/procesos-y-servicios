package app;

import components.MyMenuBar;
import components.SplashScreen;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Observable;
import java.util.Observer;
/**
 * Vista de la aplicacion
 * @author Adrian Urbano
 */
public class View implements Observer {
    private JPanel panel1;
    private JPanel centerPanel;
    private JPanel topPanel;
    private JPanel botPanel;

    JFrame frame;
    JScrollPane scrollPanel;
    JButton btStartAll;
    JButton btClearDownloads;
    JButton btStopAll;
    JButton btAdd;
    JLabel lbStatus;
    JPanel headerTopPanel;
    JPanel headerBotPanel;
    JTextField tfUrl;
    JLabel lbUrl;
    JPanel downloadsPanel;
    MyMenuBar bar;

    public View() {
        launchSplash();

        bar = new MyMenuBar();
        frame = new JFrame("View");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.setSize(600, 700);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setJMenuBar(bar);
    }

    private void launchSplash() {
        SplashScreen splashScreen = new SplashScreen();
        splashScreen.addObserver(this);

        new Thread(splashScreen).start();
    }

    public void setClosingListener(WindowListener listener) {
        frame.addWindowListener(listener);
    }

    @Override
    public void update(Observable o, Object arg) {
        frame.setVisible(true);
        frame.requestFocus();
    }
}

package app;

import components.*;
import org.joda.time.LocalTime;
import org.joda.time.Seconds;
import tasks.Download;
import tasks.DownloadBackUp;
import tasks.DownloadFactory;
import tasks.DownloadManager;
import utils.Utils;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Controlador de la aplicación
 * @author Adrian Urbano
 */
public class Controller extends WindowAdapter implements ActionListener {

    private View view;
    private Model model;
    private DownloadManager downloadManager;

    public Controller() {
        view = new View();
        model = new Model();
        downloadManager = new DownloadManager(model.getThreadsLimit());

        setListeners();
        loadUnfinishedDownloads();
    }

    private void setListeners() {
        view.btClearDownloads.addActionListener(this);
        view.btStopAll.addActionListener(this);
        view.btStartAll.addActionListener(this);
        view.btAdd.addActionListener(this);
        view.bar.setListeners(this);
        view.setClosingListener(this);
    }

    private void loadUnfinishedDownloads() {
        ArrayList<DownloadBackUp> backUps = model.getUnfinishedDownloadData();
        if (backUps.size() == 0)
            return;

        DownloadFactory factory = new DownloadFactory(this).downloadFolderPath(model.getFolderPath());
        for (DownloadBackUp backUp : backUps) {
            int downloaded = backUp.getDownloaded();
            int size = backUp.getSize();
            URL url = backUp.getUrl();

            DownloadPanel downloadPanel = factory.downloadURL(url).downloaded(downloaded).size(size).newDownload();
            downloadPanel.showProgress();
            downloadManager.submit(downloadPanel);
        }

        updateGUI();
    }

    public void updateGUI() {
        view.downloadsPanel.removeAll();
        view.tfUrl.requestFocus();
        view.tfUrl.setText("");

        for (DownloadPanel downloadPanel : downloadManager.getDownloadQueue())
            view.downloadsPanel.add(downloadPanel.getPanel());

        view.downloadsPanel.repaint();
        view.downloadsPanel.validate();
        view.downloadsPanel.updateUI();
    }

    @Override
    public void windowClosing(WindowEvent e) {
        close();
    }

    private void close() {
        if (Utils.showConfirmedMessage("Close the program ?", "Close the Program") != JOptionPane.YES_OPTION)
            return;

        clearDownloads_ActionPerformed();
        model.serializeUnfinishedDownloads(downloadManager.getUnfinishedDownloadsRefs());
        System.exit(0);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();

        if (source instanceof JMenuItem)
            menuItems_ActionPerformed(e);

        if (source instanceof JButton)
            buttons_ActionPerformed(e);
    }

    private void buttons_ActionPerformed(ActionEvent e) {
        String actionCommand = ((AbstractButton)e.getSource()).getActionCommand();

        switch (actionCommand) {
            case "add":
                add_ActionPerformed();
                break;
            case "start":
                start_ActionPerformed(e);
                break;
            case "stop":
                stop_ActionPerformed(e);
                break;
            case "cancel":
                cancel_ActionPerformed(e);
                break;
            case "startAll":
                startAll_ActionPerformed();
                break;
            case "stopAll":
                stopAll_ActionPerformed();
                break;
            case "clear":
                clearDownloads_ActionPerformed();
                break;
        }
    }

    private void add_ActionPerformed() {
        String url = view.tfUrl.getText();

        if (url.equals("")) {
            Utils.showMessage("The URL is empty");
            return;
        }

        DownloadFactory factory = new DownloadFactory(this);
        DownloadPanel download = factory.downloadURL(url).downloadFolderPath(model.getFolderPath()).newDownload();

        downloadManager.submit(download);
        updateGUI();;
    }

    private void start_ActionPerformed(ActionEvent e) {
        DownloadPanel downloadPanel = getCurrentWorkerPanel(e);

        if (downloadPanel.isManualPathSelected()) {
            model.chooseTemporalPath();
            downloadPanel.getDownload().setFilePath(Constants.TEMP_PATH);
        }

        if (downloadManager.isQueueFull())
            return;

        downloadPanel.launchDownload();
    }

    private void stop_ActionPerformed(ActionEvent e) {
        getCurrentWorkerPanel(e).getDownload().cancel();
    }

    private void cancel_ActionPerformed(ActionEvent e) {
        DownloadPanel downloadPanel = getCurrentWorkerPanel(e);

        if (downloadPanel.getDownload().isDownloading()) {
            Utils.showErrorMessage("error", "Download should be stopped first");
            return;
        }

        if (Utils.showConfirmedMessage("Delete File binded ?", "") == 0)
            deleteCanceledDownload(downloadPanel.getDownload().getFilePath());

        keepTrackOnLogs(downloadPanel.getDownload());
        downloadManager.clearDownload(downloadPanel);
        updateGUI();
    }

    private void startAll_ActionPerformed() {
        downloadManager.submitAll();
    }

    private void stopAll_ActionPerformed() {
        for (DownloadPanel downloadPanel : downloadManager.getDownloadQueue())
            downloadPanel.cancel();
    }

    private void clearDownloads_ActionPerformed() {
        for (DownloadPanel downloadPanel : downloadManager.getDownloadQueue()) {
            if (downloadPanel.isDisposable()) {
                keepTrackOnLogs(downloadPanel.getDownload());
                downloadManager.clearDownload(downloadPanel);
            }
        }

        updateGUI();
    }

    private void menuItems_ActionPerformed(ActionEvent e) {
        String actionCommand = ((AbstractButton)e.getSource()).getActionCommand();

        switch (actionCommand) {
            case "importurls":
                importURL_ActionPerformed();
                break;
            case "showlogs":
                showLogs_ActionPerformed();
                break;
            case "changepath":
                changePath_ActionPerformed();
                break;
            case "ammount":
                downloadLimit_ActionPerformed();
                break;
            case "downloadsFolder":
                openDownloadFolder_ActionPerformed();
                break;
            case "schedule":
                schedulAction_Performed();
                break;
            case "exit":
                close();
                break;
        }
    }

    public void importURL_ActionPerformed() {
        importURLfromFile();
        updateGUI();
    }

    private void showLogs_ActionPerformed() {
        ShowLogs logs = new ShowLogs(model.getDownloadsLogs(), model.getFailedDownloadLogs());
    }

    private void changePath_ActionPerformed() {
        model.chooseTemporalPath();
        model.storeTempPathOnPropertiesFile();
        model.createAppFolder();
    }

    private void downloadLimit_ActionPerformed() {
        int downloadLimit = Utils.showNumberInputMessage("Ammount of simultany downloads ?");
        if (downloadLimit == -1)
            return;

        downloadManager.setThreadsLimit(downloadLimit);
        model.storeThreadCapacityOnPropertiesFile(downloadLimit);
    }

    private void openDownloadFolder_ActionPerformed() {
        Runtime r = Runtime.getRuntime();

        try {
            Process process = r.exec("explorer.exe " + model.getFolderPath());
        } catch (Exception e) {
            System.out.println("Error al ejecutar");
        }
    }

    private void schedulAction_Performed() {
        DownloadScheduler scheduler = new DownloadScheduler();
        if (!scheduler.isScheduledCorrectly())
            return;

        URL url = scheduler.getScheduledDownloadURL();
        DownloadFactory factory = new DownloadFactory(this);
        DownloadPanel downloadPanel = factory.downloadURL(url).downloadFolderPath(model.getFolderPath()).newDownload();

        long delay = Seconds.secondsBetween(LocalTime.now(), scheduler.getTime()).getSeconds();

        Runnable futureDownload = new Runnable(){
            @Override
            public void run() {
                downloadManager.submit(downloadPanel);
                downloadPanel.launchDownload();
                updateGUI();
            }
        };
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        executor.schedule(futureDownload, delay, TimeUnit.SECONDS);

        Chronometer chrono = new Chronometer(delay, view.lbStatus);
    }

    public DownloadPanel getCurrentWorkerPanel(ActionEvent e) {
        JPanel jpanel = (JPanel) ((JButton) e.getSource()).getParent();

        for (DownloadPanel downloadPanel : downloadManager.getDownloadQueue()) {
            if (jpanel == downloadPanel.getPanel())
                return downloadPanel;
        }

        return null;
    }

    private void keepTrackOnLogs(Download download) {
        String name = download.getName();
        boolean downloadState = download.isSuccessfullDownload();

        model.updateLogs(name, downloadState);
    }

    private void deleteCanceledDownload(String path) {
        File descarga = new File(path);
        descarga.delete();
    }

    private void importURLfromFile() {
        DownloadFactory factory = new DownloadFactory(this).downloadFolderPath(model.getFolderPath());

        for (String url : model.getExternalUrls()) {
            DownloadPanel downloadPanel = factory.downloadURL(url).newDownload();
            downloadManager.submit(downloadPanel);
        }
    }

}

package app;

import tasks.DownloadBackUp;
import utils.Utils;
import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Properties;

import static app.Constants.*;
/**
 * Modelo de la aplicacion
 * @author Adrian Urbano
 */
public class Model {

    public Model() {
        File props = new File(DEFAULT_PROPERTIES_FILE_PATH);
        if (!props.exists())
            createPropsFile();

        File appFolder = new File(getFolderPath());
        if (!appFolder.exists())
            createAppFolder();
    }

    public String getFolderPath() {
        String folder = getProperty("folder");
        return folder;
    }

    public int getThreadsLimit() {
        String limit = getProperty("capacity");
        return Integer.parseInt(limit);
    }

    private String getProperty(String propertyName) {
        Properties props = new Properties();

        try {
            props.load(new FileInputStream(DEFAULT_PROPERTIES_FILE_PATH));
        } catch (IOException e) {
            Utils.showErrorMessage("error", "properties file not found");
        }

      return props.getProperty(propertyName);
    }

    private void createPropsFile() {
        Properties properties = new Properties();
        properties.setProperty("folder", DEFAULT_DOWNLOADS_FOLDER_PATH);
        properties.setProperty("capacity", DEFAULT_THREADS_LIMIT_SIZE);

        try {
            properties.store(new FileOutputStream(DEFAULT_PROPERTIES_FILE_PATH), "properties");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void createAppFolder() {
        File folder = new File(getFolderPath());
        folder.mkdirs();

        File downloadLogs = new File(DOWNLOADS_LOGS_PATH);
        if (!downloadLogs.exists())
            createLogs();

        File failedDownloadLogs = new File(FAILED_DOWNLOADS_LOGS_PATH);
        if (!failedDownloadLogs.exists()) {
            createFailedLogs();
        }

        File unfinishedDownload = new File(UNFINISHED_DOWNLOAD_DATA_PATH);
        if (!unfinishedDownload.exists()) {
            try {
                unfinishedDownload.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void createLogs() {
        storeDownloadLog(DOWNLOADS_LOGS_PATH, "");
    }

    private void createFailedLogs() {
        storeDownloadLog(FAILED_DOWNLOADS_LOGS_PATH, "");
    }

    public void updateLogs(String downloadName, boolean successDownload) {
        if (!successDownload) {
            storeDownloadLog(FAILED_DOWNLOADS_LOGS_PATH, downloadName);
            return;
        }

        storeDownloadLog(DOWNLOADS_LOGS_PATH, downloadName);
    }

    private void storeDownloadLog(String filePath, String line) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath, true))) {
            writer.write(line);
            writer.newLine();
        } catch (IOException e) {
            Utils.showErrorMessage("error", "File not Found");
        }
    }

    public void chooseTemporalPath() {
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setDialogTitle("Save");
        chooser.setApproveButtonText("Save as..");

        if (chooser.showOpenDialog(null) == JFileChooser.CANCEL_OPTION)
            return;

        File directory = chooser.getSelectedFile();
        Constants.TEMP_PATH = directory.getPath() + File.separator;
    }

    private void chooseUrlsFilePath() {
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        chooser.setDialogTitle("Import");
        chooser.setApproveButtonText("Choose file..");

        if (chooser.showOpenDialog(null) == JFileChooser.CANCEL_OPTION)
            return;

        File file = chooser.getSelectedFile();
        TEMP_PATH = file.getPath();
    }

    public void storeTempPathOnPropertiesFile() {
        Properties properties = new Properties();

        try {
            properties.load(new FileInputStream(DEFAULT_PROPERTIES_FILE_PATH));
            properties.setProperty("folder", TEMP_PATH);
            properties.store(new FileOutputStream(DEFAULT_PROPERTIES_FILE_PATH), "properties File");
        } catch (IOException e) {
            Utils.showErrorMessage("error", "Properties File not Found");
        }
    }

    public void storeThreadCapacityOnPropertiesFile(int downloadLimit) {
        Properties properties = new Properties();

        try {
            properties.load(new FileInputStream(DEFAULT_PROPERTIES_FILE_PATH));
            properties.setProperty("capacity", String.valueOf(downloadLimit));
            properties.store(new FileOutputStream(DEFAULT_PROPERTIES_FILE_PATH), "properties File");
        } catch (IOException e) {
            Utils.showErrorMessage("error", "Properties File not Found");
        }
    }

    public ArrayList<String> getExternalUrls() {
        chooseUrlsFilePath();
        ArrayList<String> urls = extractPlainTextLines(TEMP_PATH);
        return urls;
    }

    public String[] getDownloadsLogs() {
        ArrayList<String> downloads = extractPlainTextLines(DOWNLOADS_LOGS_PATH);
        return downloads.toArray(new String[0]);
    }

    public String[] getFailedDownloadLogs() {
        ArrayList<String> downloads = extractPlainTextLines(FAILED_DOWNLOADS_LOGS_PATH);
        return downloads.toArray(new String[0]);
    }

    public ArrayList<String> extractPlainTextLines(String filePath) {
        ArrayList<String> strings = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {

            String linea = reader.readLine();
            while (linea != null) {
                strings.add(linea);
                linea = reader.readLine();
            }
        } catch (IOException e) {
            Utils.showErrorMessage("error", "Invalid File");
        }

        return strings;
    }

    public void serializeUnfinishedDownloads(ArrayList<DownloadBackUp> backUpList) {
        try (ObjectOutputStream writer = new ObjectOutputStream(new FileOutputStream(UNFINISHED_DOWNLOAD_DATA_PATH))) {
           writer.writeObject(backUpList);
        } catch (IOException e) {
           Utils.showErrorMessage("error", "Failed to serialize the unfinished downloads");
        }
    }

    public ArrayList<DownloadBackUp> getUnfinishedDownloadData() {
        Object object = null;
        try ( ObjectInputStream reader = new ObjectInputStream(new FileInputStream(UNFINISHED_DOWNLOAD_DATA_PATH)) ) {
            object = reader.readObject();
        } catch (Exception e) {
            object = new ArrayList<DownloadBackUp>();
        }

        return (ArrayList<DownloadBackUp>) object;
    }

}

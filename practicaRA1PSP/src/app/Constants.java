package app;

import java.io.File;

/**
 * Created by adriu on 17/11/2015.
 */
public class Constants {

    public static final String DEFAULT_PROPERTIES_FILE_PATH =
            System.getProperty("user.home") + File.separator + "dmanager.props";

    public static final String DEFAULT_DOWNLOADS_FOLDER_PATH =
            System.getProperty("user.home") + File.separator + "dmanager" + File.separator + "DAMloads" + File.separator;

    public static final String DOWNLOADS_LOGS_PATH =
            System.getProperty("user.home") + File.separator + "dmanager/slog.txt";

    public static final String FAILED_DOWNLOADS_LOGS_PATH =
            System.getProperty("user.home") + File.separator + "dmanager/flog.txt";

    public static final String UNFINISHED_DOWNLOAD_DATA_PATH =
            System.getProperty("user.home") + File.separator + "dmanager/dmanager.dat";

    public static final String DEFAULT_THREADS_LIMIT_SIZE = "3";

    public static String TEMP_PATH = System.getProperty("user.home") + File.separator;

}

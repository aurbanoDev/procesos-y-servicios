package utils;

import app.Constants;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.stream.Stream;

/**
 * Utilidades
 * @author Adrian Urbano
 */
public class Utils {

    public static void showMessage(String message) {
        JOptionPane.showMessageDialog(null, message, "", JOptionPane.NO_OPTION);
    }

    public static void showErrorMessage(String title, String message) {
        JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
    }

    public static String showInputMessage(String message) {
        return JOptionPane.showInputDialog(null, message);
    }

    public static int showNumberInputMessage(String message) {

        try {
            int number = Integer.parseInt(showInputMessage(message));
            if (number <= 0)
                throw new NumberFormatException();

            return number;
        } catch (NumberFormatException e) {
            showMessage("Number higher than 0 expected");
        }

        return -1;
    }

    public static int showConfirmedMessage(String title, String message) {
        return JOptionPane.showConfirmDialog(null, title , message , JOptionPane.YES_NO_CANCEL_OPTION);
    }

    public static String getFileNameFromURL(URL url) {
        String fileName = url.getFile();
        return fileName.substring(fileName.lastIndexOf('/') + 1);
    }


    public static void closeStreams(Closeable... streams) {
        for (Closeable stream : streams) {
            try {
                stream.close();
            } catch (IOException e) {
                showErrorMessage("error", "Failed closing the streams");
            }
        }
    }

    public static Color getDefaultForegroundColor() {
        return new JProgressBar().getForeground();
    }
}

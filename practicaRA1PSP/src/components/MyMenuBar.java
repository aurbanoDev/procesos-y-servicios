package components;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Menu bar personalizada
 * @author Adrian Urbano
 */
public class MyMenuBar extends JMenuBar {

    private JMenu file;
    private JMenu settings;

    private JMenuItem exit;
    private JMenuItem schedule;
    private JMenuItem changePath;
    private JMenuItem showLogs;
    private JMenuItem importUrls;
    private JMenuItem ammountDownloads;
    private JMenuItem openDownloadFolder;


    public MyMenuBar() {
        file = new JMenu("File");
        settings = new JMenu("Settings");
        schedule = new JMenuItem("Schedule a download");
        exit = new JMenuItem("Exit");
        changePath = new JMenuItem("Change Path");
        showLogs = new JMenuItem("show Logs");
        importUrls = new JMenuItem("import Urls");
        ammountDownloads = new JMenuItem("Number Of Downloads");
        openDownloadFolder = new JMenuItem("Open Downloads Folder");

        exit.setActionCommand("exit");
        changePath.setActionCommand("changepath");
        showLogs.setActionCommand("showlogs");
        importUrls.setActionCommand("importurls");
        ammountDownloads.setActionCommand("ammount");
        openDownloadFolder.setActionCommand("downloadsFolder");
        schedule.setActionCommand("schedule");

        file.add(importUrls);
        file.add(showLogs);
        file.add(schedule);
        file.add(openDownloadFolder);
        file.add(exit);

        settings.add(changePath);
        settings.add(ammountDownloads);

        add(file);
        add(settings);
    }

    public void setListeners(ActionListener listener) {
        exit.addActionListener(listener);
        changePath.addActionListener(listener);
        showLogs.addActionListener(listener);
        importUrls.addActionListener(listener);
        ammountDownloads.addActionListener(listener);
        openDownloadFolder.addActionListener(listener);
        schedule.addActionListener(listener);
    }
}

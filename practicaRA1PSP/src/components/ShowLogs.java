package components;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;

/**
 * Dialogo que contiene una tabla para listar los nombres de las descargas (fallidas y exitosas).
 * @author Adrian Urbano
 */
public class ShowLogs extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JPanel centerPanel;
    private JTable logTable;
    private DefaultTableModel model;

    public ShowLogs(String[] downloads , String[] failedDownloads) {
        int rowCount = Math.max(downloads.length, failedDownloads.length);
        model = new DefaultTableModel(rowCount , 0);
        logTable.setModel(model);

        fillTable(downloads, failedDownloads);
        setFuctions();

        setContentPane(contentPane);
        getRootPane().setDefaultButton(buttonOK);
        setTitle("Downloads Logs");
        setSize(450, 320);
        setLocationRelativeTo(null);
        setModal(true);
        setVisible(true);
    }

    private void fillTable(String[] downloads, String[] failedDownloads) {
        model.addColumn("Downloads", downloads);
        model.addColumn("Failed Downloads", failedDownloads);
    }

    private void setFuctions() {
        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

    }

    private void onOK() {
        dispose();
    }

    private void onCancel() {
        dispose();
    }

}

package components;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;

/**
 * Splash screen
 * @author Adrian Urbano
 */
public class SplashScreen extends Observable implements Runnable {

    private JWindow frame;

    @Override
    public void run() {
        launchSplash();
    }

    private void launchSplash() {
        JLabel label = new JLabel(new ImageIcon("res/splashGratis.png"));
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(label, BorderLayout.CENTER);

        frame = new JWindow();
        frame.setContentPane(panel);
        frame.setSize(new Dimension(540, 320));
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        disposeSplash();
    }

    private void disposeSplash() {
        try {
            Thread.sleep(4500);
            frame.dispose();
        } catch (InterruptedException e) {}

        setChanged();
        notifyObservers();
    }

}

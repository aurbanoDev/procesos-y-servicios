package components;

import org.joda.time.LocalTime;
import utils.Utils;

import javax.swing.*;
import java.awt.event.*;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Interfaz Gr�fica encargada de recoger los datos para programar una futura descarga.
 * Contiene metodos para recuperar la URL y la hora programada de la descarga.
 * Con el m�todo isScheduledCorrectly se valida si se recogieron los datos correctamente.
 * @author Adrian Urbano
 */
public class DownloadScheduler extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField tfUrl;
    private TimePicker timePicker;

    private LocalTime scheduledTime;
    private URL scheduledDownloadURL;
    private boolean scheduledCorrectly;

    public DownloadScheduler() {
        setTitle("Download Scheduler");
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setSize(450, 190);
        setLocationRelativeTo(null);
        setFuctions();
        setVisible(true);
    }

    private void setFuctions() {
        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

    }

    private void onOK() {
        String url = tfUrl.getText();

        if (url.equals("")) {
            Utils.showErrorMessage("Error", "The URL is empty");
            return;
        }

        try {
            scheduledDownloadURL = new URL(tfUrl.getText());
            scheduledTime = timePicker.getTime();
        } catch (MalformedURLException e) {
            Utils.showErrorMessage("Error", "URL is not found");
            return;
        }

        scheduledCorrectly = true;
        dispose();
    }

    private void onCancel() {
        scheduledCorrectly = false;
        dispose();
    }

    public LocalTime getTime() {
        return scheduledTime;
    }

    public URL getScheduledDownloadURL() {
      return scheduledDownloadURL;
    }

    public boolean isScheduledCorrectly() {
        return scheduledCorrectly;
    }
}

package components;

import com.toedter.components.JSpinField;
import org.joda.time.*;
import javax.swing.*;
import java.awt.*;


/**
 * Componente que permite recoger una hora en formato (h,m,s). La hora se obtiene con el m�todo getTime.
 * @author Adrian Urbano
 * @see LocalTime
 */
public class TimePicker extends JPanel {

    private JSpinField hoursSpinField;
    private JSpinField minutesSpinfield;
    private JSpinField secondSpinfield;

    public TimePicker() {
        setLayout(new FlowLayout(FlowLayout.CENTER, 10 , 10));
        hoursSpinField = getSpinField("hoursSpinField", 0, 23);
        minutesSpinfield = getSpinField("minutesSpinfield", 0, 59);
        secondSpinfield = getSpinField("secondSpinfield", 0, 59);

        add(new JLabel("Hours"));
        add(hoursSpinField);
        add(new JLabel("Minutes"));
        add(minutesSpinfield);
        add(new JLabel("Seconds"));
        add(secondSpinfield);
    }

    private JSpinField getSpinField(String tooltipText, int min, int max) {
        JSpinField spinField = new JSpinField(min , max);
        spinField.setToolTipText(tooltipText);
        spinField.adjustWidthToMaximumValue();

        return spinField;
    }

    public LocalTime getTime() {
        int hours = hoursSpinField.getValue();
        int minutes = minutesSpinfield.getValue();
        int seconds = secondSpinfield.getValue();

        LocalTime time = new LocalTime(hours, minutes, seconds);
        return time;
    }
}

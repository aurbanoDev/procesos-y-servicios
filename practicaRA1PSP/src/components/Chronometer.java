package components;

import javax.swing.*;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Clase que representa un cron�metro en cuenta regresiva con actualizaciones con precisi�n de un segundo.
 * @author Adrian Urbano
 */
public class Chronometer {

    private JLabel progressLabel;
    private Timer timer;
    private long secondsWorking;

    public Chronometer(long secondsWorking, JLabel progressLabel) {
        this.secondsWorking = secondsWorking;
        this.progressLabel = progressLabel;
        timer = new Timer();

        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                updateChrono();
            }
        };
        timer.schedule(task, 0, 1000);
    }

    private void updateChrono() {

        if (secondsWorking > 0) {
            progressLabel.setText(String.valueOf("Download Scheduled:  " + getRemainingTime()) + "                    ");
            secondsWorking--;
            return;
        }

        timer.cancel();
        progressLabel.setText("");
    }

    private StringBuffer getRemainingTime() {
        int timeOnSeconds = (int) secondsWorking;
        int hours = timeOnSeconds / 3600;
        int minutes = (timeOnSeconds - (3600 * hours)) / 60;
        int seconds = timeOnSeconds - ((hours * 3600) + (minutes * 60));

        String h = (hours < 10) ? "0" + hours : String.valueOf(hours);
        String m = (minutes < 10) ? "0" + minutes : String.valueOf(minutes);
        String s = (seconds < 10) ? "0" + seconds : String.valueOf(seconds);

        return new StringBuffer(h).append(" : ").append(m).append(" : ").append(s);
    }
}

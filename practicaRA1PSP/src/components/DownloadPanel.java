package components;

import tasks.Download;
import utils.Utils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.EventListener;

/**
 * Componente para monitorizar gráficamente una descarga con una progress bar.
 * @author Adrian Urbano
 */
public class DownloadPanel implements PropertyChangeListener {

    private JProgressBar bar;
    private JPanel panel;
    private JButton btStart;
    private JButton btStop;
    private JButton cancelButton;
    private JLabel lbCurrentSize;
    private JLabel lbDownloadName;
    private JLabel lbStatus;
    private JCheckBox checkSelectPath;

    private Download download;

    public void setListeners(EventListener listener) {
        btStart.addActionListener((ActionListener) listener);
        btStop.addActionListener((ActionListener) listener);
        cancelButton.addActionListener((ActionListener) listener);
        download.addPropertyChangeListener(this);
    }

    public JPanel getPanel() {
        return panel;
    }

    public void setDownload(Download download) {
        this.download = download;
        lbDownloadName.setText(download.getName());
    }

    public Download getDownload() {
        return download;
    }

    public void launchDownload() {
        download.start();
        checkSelectPath.setEnabled(false);
        bar.setForeground(Utils.getDefaultForegroundColor());
    }

    public void cancel() {
        download.cancel();
    }

    public boolean isManualPathSelected() {
        return checkSelectPath.isSelected();
    }

    public boolean isDisposable() {
        if (download.getStatus() == Download.COMPLETED)
            return true;

        return false;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        String propertyFired = evt.getPropertyName();

        if (propertyFired.equals("progress"))
            showProgress();

        if (propertyFired.equals("complete"))
            showDownloadStatus();
    }

    private void showDownloadStatus() {
        Color color = (download.isSuccessfullDownload()) ? Color.GREEN : Color.RED;
        String status = (download.isSuccessfullDownload()) ? "Complete" :"Canceled";
        bar.setForeground(color);
        lbStatus.setText(status);
    }

    public void showProgress() {
        lbCurrentSize.setText(download.getProgressOnMB());
        bar.setValue(download.getDownloadedPercent());
        bar.setString(download.getDownloadedPercent() + "%");
    }



}

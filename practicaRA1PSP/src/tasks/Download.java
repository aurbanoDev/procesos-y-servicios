package tasks;

import utils.Utils;

import javax.swing.*;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Observable;

/**
 * Clase que representa la descarga reanudable de una URL.
 * La descarga se realiza a traves de un swing worker , notificando su progreso en el hilo de eventos.
 * Contiene metodos de interes como el estado actual de la descarga (descargando , esperando, cancelada) , el progreso
 * en MB, el porcentaje descargado o el exito o fracaso de la descarga.
 * @author Adrian Urbano
 */
public class Download extends Observable {

    private static final int BUFFER_SIZE = 1024;
    public static final int DOwNLOADING = 0;
    public static final int COMPLETED = 1;
    public static final int CANCELED = 2;
    public static final int WAITING = 4;
    public static final int ERROR = 3;

    private SwingWorker<Void, Integer> downloadWorker;
    private PropertyChangeListener UIpanelListener;

    private URL url;
    private String filePath;
    private int size;
    private int status;
    private int downloaded;
    
    public Download(URL url, String filePath) {
        this(url, filePath, 0, 0);
    }

    public Download(URL url, String filePath, int downloaded, int size) {
        this.url = url;
        this.size = size;
        this.downloaded = downloaded;
        this.filePath = filePath + File.separator + Utils.getFileNameFromURL(url);

        status = WAITING;
        prepareWorker();
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        UIpanelListener = listener;
    }

    public String getFilePath() {
        return filePath;
    }

    public String getName() {
        return Utils.getFileNameFromURL(url);
    }

    public boolean isSuccessfullDownload() {
        return (status == COMPLETED) ? true : false ;
    }

    public boolean isDownloading() {
        return (status == DOwNLOADING) ? true : false;
    }

    public boolean isWaiting() {
      return (status == WAITING) ? true : false;
    }

    public boolean isCancelled() {
        return (status == CANCELED) ? true : false;
    }

    public int getStatus() {
        return status;
    }

    public int getDownloadedPercent() {
        try {
            return downloaded * 100 / size;
        } catch (ArithmeticException e) {
            return 0;
        }
    }

    public String getProgressOnMB() {
        String progressOnMB =  String.format("%.3g%n", (float)downloaded / 1048576);
        String sizeOnMB =  String.format("%.3g%n", (float)size / 1048576 ) + "  Mb";

        return progressOnMB + "  /  " + sizeOnMB;
    }

    public DownloadBackUp getBackUp() {
        System.out.println(size);
        return new DownloadBackUp(url, downloaded, size);
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath + File.separator + Utils.getFileNameFromURL(url);
    }

    public void start() {
        status = DOwNLOADING;
        notifyNewStatus();
        prepareWorker();
        downloadWorker.execute();
    }

    public void cancel() {
        status = CANCELED;
        downloadWorker.cancel(true);
        notifyNewStatus();
    }

    private byte[] requestBuffer() {
        int diff = size - downloaded;
        return (diff > BUFFER_SIZE) ? new byte[BUFFER_SIZE] : new byte[diff];
    }

    private void notifyNewStatus() {
        setChanged();
        notifyObservers();
    }

    private void prepareWorker() {
        downloadWorker = new SwingWorker<Void, Integer>() {
            @Override
            protected Void doInBackground() {

                RandomAccessFile raf =null;
                InputStream stream = null;

                while (!isCancelled()) {

                    try {
                        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                        connection.setRequestProperty("Range", "bytes=" + downloaded + "-");
                        connection.connect();

                        if (connection.getContentLength() > size)
                            size = connection.getContentLength();

                        stream = connection.getInputStream();
                        raf = new RandomAccessFile(filePath, "rw");
                        raf.seek(downloaded);

                        while (!isCancelled()) {
                            byte buffer[] = requestBuffer();

                            int read = stream.read(buffer);
                            if (read == -1)
                                break;

                            raf.write(buffer, 0, read);
                            downloaded += read;
                            setProgress(getDownloadedPercent());
                        }

                    } catch(Exception ex) {
                        status = ERROR;
                        notifyNewStatus();

                        if (ex instanceof MalformedURLException)
                            Utils.showErrorMessage("Error", "URL is not found");

                        if (ex instanceof IOException)
                            Utils.showErrorMessage("Error", "Connection Failed");

                    } finally {
                        Utils.closeStreams(raf, stream);
                        cancel(true);
                    }
                }

                if (status == DOwNLOADING) {
                    status = COMPLETED;
                    notifyNewStatus();
                }

                firePropertyChange("complete",  false, true);
                return null;
            }
        };

        downloadWorker.addPropertyChangeListener(UIpanelListener);
    }

}

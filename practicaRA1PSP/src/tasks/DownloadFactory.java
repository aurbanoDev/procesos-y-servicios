package tasks;

import components.DownloadPanel;
import utils.Utils;

import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.net.URL;
import static app.Constants.DEFAULT_DOWNLOADS_FOLDER_PATH;

/**
 * Factoria para la creacion del panel gr�fico con la descarga.
 * @author Adrian Urbano
 */
public class DownloadFactory {

    private URL downloadUrl;
    private String downloadFolderPath;
    private ActionListener listener;

    private int size;
    private int downloaded;

    public DownloadFactory(ActionListener listener) {
        this.listener = listener;
        downloadFolderPath = DEFAULT_DOWNLOADS_FOLDER_PATH;
    }

    public DownloadFactory downloadURL(URL downloadUrl) {
        this.downloadUrl = downloadUrl;
        return this;
    }

    public DownloadFactory downloadURL(String url) {
        try {
            downloadUrl = new URL(url);
        } catch (MalformedURLException e) {
            Utils.showErrorMessage("error", "URL is not found");
        }

        return this;
    }

    public DownloadFactory downloadFolderPath(String downloadFolderPath) {
        this.downloadFolderPath = downloadFolderPath;
        return this;
    }

    public DownloadFactory downloaded(int downloaded) {
        this.downloaded = downloaded;
        return this;
    }

    public DownloadFactory size(int size) {
        this.size = size;
        return this;
    }

    public DownloadFactory listener(ActionListener listener) {
        this.listener = listener;
        return this;
    }

    public DownloadPanel newDownload() {
        Download download = new Download(downloadUrl, downloadFolderPath, downloaded, size);
        size = 0;
        downloaded = 0;

        DownloadPanel downloadPanel = new DownloadPanel();
        downloadPanel.setDownload(download);
        downloadPanel.setListeners(listener);

        return downloadPanel;
    }
}

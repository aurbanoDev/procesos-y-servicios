package tasks;

import components.DownloadPanel;
import org.joda.time.DateTime;
import org.joda.time.Seconds;

import java.util.*;
import java.util.concurrent.*;

/**
 * Clase encargada de controlar el flujo de descargas.
 * @author Adrian Urbano
 */
public class DownloadManager implements Observer {

    private CopyOnWriteArrayList<DownloadPanel> downloadQueue;
    private int activeDownloads;
    private int threadsLimit;

    public DownloadManager(int threads) {
        downloadQueue = new CopyOnWriteArrayList<>();
        threadsLimit = threads;
        activeDownloads = 0;
    }

    public void submit(DownloadPanel downloadPanel) {
        downloadPanel.getDownload().addObserver(this);
        downloadQueue.add(downloadPanel);
    }

    public void submitAll() {
        int idle = threadsLimit - activeDownloads;
        int launchedDownloads = 0;

        for (DownloadPanel download : downloadQueue) {
            if (launchedDownloads == idle)
                return;

            if (download.getDownload().isWaiting()) {
                download.launchDownload();
                launchedDownloads++;
            }
        }
    }

    public void clearDownload(DownloadPanel download) {
        downloadQueue.remove(download);
    }

    public void setThreadsLimit(int threadsLimit) {
        this.threadsLimit = threadsLimit;
    }

    public boolean isQueueFull() {
        return activeDownloads == threadsLimit;
    }

    public ArrayList<DownloadBackUp> getUnfinishedDownloadsRefs() {
        ArrayList<DownloadBackUp> unfinishedDownloads = new ArrayList<>();

        for (DownloadPanel downloadPanel : downloadQueue) {
            if (!downloadPanel.isDisposable())
                unfinishedDownloads.add(downloadPanel.getDownload().getBackUp());
        }

        return unfinishedDownloads;
    }

    public CopyOnWriteArrayList<DownloadPanel> getDownloadQueue() {
        return downloadQueue;
    }

    @Override
    public void update(Observable o, Object arg) {
        Download download = (Download) o;

        switch (download.getStatus()) {
            case Download.DOwNLOADING:
                activeDownloads++;
                return;
            case Download.COMPLETED:
                activeDownloads--;
                break;
            case Download.CANCELED:
                activeDownloads--;
                break;
        }

        submitAll();
    }
}

package tasks;

import java.io.Serializable;
import java.net.URL;

/**
 * Clase que contiene la informacion necesaria para reanudar una descarga sin perder el progreso.
 * @author Adrian Urbano
 */
public class DownloadBackUp implements Serializable {

    private URL url;
    private int size;
    private int downloaded;

    public DownloadBackUp(URL url, int downloaded, int size) {
        this.url = url;
        this.downloaded = downloaded;
        this.size = size;
    }

    public URL getUrl() {
        return url;
    }

    public int getDownloaded() {
        return downloaded;
    }

    public int getSize() {
        return size;
    }
}

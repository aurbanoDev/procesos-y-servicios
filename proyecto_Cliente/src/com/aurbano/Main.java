package com.aurbano;

import com.aurbano.app.Controlador;
import com.aurbano.app.Ventana;

import javax.swing.*;

/**
 * Created by adriu on 20/02/2016.
 */
public class Main {

    public static void main(String[] args) {

        try {
            UIManager.setLookAndFeel("com.jtattoo.plaf.aluminium.AluminiumLookAndFeel");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        new Controlador(new Ventana());
    }
}

package com.aurbano.renderer;

import javax.swing.*;
import java.awt.*;

/**
 * Created by adriu on 20/02/2016.
 */
public class UsuarioCellRenderer extends DefaultListCellRenderer {

    private Font fuente;

    public UsuarioCellRenderer() {
        fuente = new Font("helvitica", Font.BOLD, 18);
    }

    public UsuarioCellRenderer(Font fuente) {
        this.fuente = fuente;
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {

        JLabel label = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

        ImageIcon icon = new ImageIcon("res/on.png");
        label.setIcon(icon);
        label.setHorizontalTextPosition(JLabel.RIGHT);
        label.setFont(fuente);

        return label;
    }
}

package com.aurbano.gui;

import com.aurbano.utilidades.Utils;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RegistroDialog extends JDialog implements ActionListener {

    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField TFusuario;
    private JTextField TFcontrasena;

    private String usuario;
    private String contrasena;

    private boolean cancelado;

    public RegistroDialog() {
        setContentPane(contentPane);
        getRootPane().setDefaultButton(buttonOK);
        setModal(true);
        pack();
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        setListeners();
        setVisible(false);
    }

    public void setListeners() {
        buttonOK.addActionListener(this);
        buttonCancel.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == buttonOK) {
            usuario = TFusuario.getText();
            contrasena = TFcontrasena.getText();

            if (usuario.equals("")) {
                Utils.mostrarMensajeError("Introduce tu nick", "Error");
                return;
            }

            setVisible(false);
        }

        if (e.getSource() == buttonCancel) {
            cancelado = true;
            setVisible(false);
        }

        TFusuario.setText("");
        TFcontrasena.setText("");
        TFusuario.requestFocus();
    }

    public String getUsuario() {
        return usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public boolean isCancelado() {
        return cancelado;
    }
}

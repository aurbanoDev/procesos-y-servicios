package com.aurbano.gui;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultCaret;
import javax.swing.text.Document;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowListener;
import java.util.Arrays;
import java.util.List;

/**
 * Created by adriu on 21/02/2016.
 */
public class SalaPrivado extends KeyAdapter {

    JPanel panel;
    JTextPane JEtexto;
    JTextField TFsalida;
    JLabel LBparticipante1;
    JLabel LBparticipante2;
    JButton BTenviar;
    JFrame frame;

    public SalaPrivado(ActionListener listener) {
        DefaultCaret caret = (DefaultCaret)JEtexto.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

        frame = new JFrame();
        frame.setContentPane(panel);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.pack();

        setListeners(listener);
        frame.setVisible(true);
    }

    public void setListeners(ActionListener listener) {
        TFsalida.addKeyListener(this);
        BTenviar.addActionListener(listener);
        frame.addWindowListener((WindowListener) listener);
    }

    public void setParticipantes(String host, String invitado) {
        frame.setTitle(invitado);
        LBparticipante1.setText(host);
        LBparticipante2.setText(invitado);
        BTenviar.setActionCommand(invitado);
    }

    public void actualizarEntrada(String texto) {
        try {
            Document documento = JEtexto.getDocument();
            documento.insertString(documento.getLength(), texto +"\n", null);
        } catch (BadLocationException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getKeyChar() == KeyEvent.VK_ENTER)
            BTenviar.doClick();
    }

    public String getMensaje() {
        return TFsalida.getText();
    }

    public void limpiarTextField() {
        TFsalida.setText("");
    }

    public List<String> getConversacion() {
        String[] lineas = new String[0];

        try {
            lineas = JEtexto.getDocument().getText(0, JEtexto.getDocument().getLength()).split("\n");
        } catch (BadLocationException e) {
            e.printStackTrace();
        }

        return Arrays.asList(lineas);
    }
}

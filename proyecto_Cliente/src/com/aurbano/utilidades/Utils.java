package com.aurbano.utilidades;

import javax.swing.*;
import java.io.*;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;

/**
 * Created by adriu on 20/02/2016.
 */
public class Utils {

    public static void mostrarMensaje(String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje);
    }

    public static void mostrarMensajeError(String mensaje, String titulo) {
        JOptionPane.showMessageDialog(null, mensaje, titulo, JOptionPane.ERROR_MESSAGE);
    }

    public static String mostrarMensajeConInput(String mensaje) {
        return JOptionPane.showInputDialog(null, mensaje);
    }

    public static int mostrarMensajeConfirmacion(String mensaje, String titulo) {
        return JOptionPane.showConfirmDialog(null, mensaje, titulo, JOptionPane.OK_OPTION);
    }

    public static int mostrarMensajeConInputNumerico(String mensaje) {
        try {
            int number = Integer.parseInt(mostrarMensajeConInput(mensaje));
            if (number <= 0)
                throw new NumberFormatException();

            return number;
        } catch (NumberFormatException e) {
            mostrarMensaje("Se esperaba un numero mayor de 0");
        }

        return -1;
    }

    public static List<String> formatearLista(String listaUsuarios) {
        listaUsuarios = listaUsuarios.replaceAll(" ", "");
        listaUsuarios = listaUsuarios.replace("[", "");
        listaUsuarios = listaUsuarios.replace("]", "");

        return Arrays.asList(listaUsuarios.split(","));
    }

    public static void crearFicheroPropiedades() {
        Properties configuracion = new Properties();

        try {
            configuracion.setProperty("logs", "no");
            configuracion.setProperty("ip", "localhost");
            configuracion.store(new FileOutputStream(Constantes.PATH_PROPS), "fichero propiedades");
        } catch (IOException e) {
            System.out.println("error");
        }
    }

    public static String getProperty(String nombre) {
        Properties properties = new Properties();

        try {
            properties.load(new FileInputStream(Constantes.PATH_PROPS));
        } catch (IOException e) {
            System.out.println("no se encontro el finchero de propiedades");
        }

        return properties.getProperty(nombre);
    }

    public static void setProperty(String property, String valor) {
        Properties properties = new Properties();

        try {
            properties.load(new FileInputStream(Constantes.PATH_PROPS));
            properties.setProperty(property, valor);
            properties.store(new FileOutputStream(Constantes.PATH_PROPS), "Fichero Propiedades");
        } catch (IOException e) {
            Utils.mostrarMensaje("no se encontro el finchero de propiedades");
        }
    }

    public static boolean  logsActivos() {
        String propiedad = getProperty("logs");
        if (propiedad.equalsIgnoreCase("si"))
            return true;

        return false;
    }

    public static void setLogs(boolean activo) {
        Properties properties = new Properties();
        String valor = (activo) ? "si" : "no";

        try {
            properties.load(new FileInputStream(Constantes.PATH_PROPS));
            properties.setProperty("logs", valor);
            properties.store(new FileOutputStream(Constantes.PATH_PROPS), "fichero propiedades");
        } catch (IOException e) {
            System.out.println("error");
        }
    }

    public static void guardarLog(String invitado, List<String> log) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(Constantes.PATH_LOGS + invitado + ".txt", true))) {

            writer.write(String.valueOf(new GregorianCalendar().getTime()));
            writer.newLine();

            for (String linea : log) {
                writer.write(linea);
                writer.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

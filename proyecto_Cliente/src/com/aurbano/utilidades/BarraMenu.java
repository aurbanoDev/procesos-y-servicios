package com.aurbano.utilidades;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Created by adriu on 20/02/2016.
 */
public class BarraMenu extends JMenuBar {

    private JMenu archivo;
    private JMenu preferencias;
    private JMenu guardarLogs;

    private JMenuItem si;
    private JMenuItem no;
    private JMenuItem host;
    private JMenuItem monitor;
    private JMenuItem conectar;
    private JMenuItem desconectar;
    private JMenuItem registrarse;
    private JMenuItem salir;

    public BarraMenu() {
        inicializar();

        archivo.add(conectar);
        archivo.add(desconectar);
        archivo.add(registrarse);
        archivo.add(salir);
        preferencias.add(host);
        preferencias.add(monitor);
        preferencias.add(guardarLogs);
        guardarLogs.add(si);
        guardarLogs.add(no);

        add(archivo);
        add(preferencias);
    }

    public void inicializar() {
        archivo = new JMenu("Archivo");
        preferencias = new JMenu("Preferencias");
        guardarLogs = new JMenu("Guardar Registros");

        si = new JMenuItem("Si");
        no = new JMenuItem("No");
        salir = new JMenuItem("Salir");
        host = new JMenuItem("Direccion IP");
        conectar = new JMenuItem("Conectar");
        registrarse = new JMenuItem("Registrarse");
        desconectar = new JMenuItem("Desconectar");
        monitor = new JMenuItem("Monitorizar Servidor");


        si.setActionCommand("si");
        no.setActionCommand("no");
        host.setActionCommand("host");
        salir.setActionCommand("salir");
        guardarLogs.setActionCommand("registros");
        monitor.setActionCommand("monitor");
        archivo.setActionCommand("archivo");
        conectar.setActionCommand("conectar");
        registrarse.setActionCommand("registrarse");
        desconectar.setActionCommand("desconectar");
    }

    public void setListeners(ActionListener listener) {
        si.addActionListener(listener);
        no.addActionListener(listener);
        host.addActionListener(listener);
        salir.addActionListener(listener);
        monitor.addActionListener(listener);
        conectar.addActionListener(listener);
        registrarse.addActionListener(listener);
        desconectar.addActionListener(listener);
        guardarLogs.addActionListener(listener);
    }
}

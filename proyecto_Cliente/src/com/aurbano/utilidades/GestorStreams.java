package com.aurbano.utilidades;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by adriu on 21/02/2016.
 */
public class GestorStreams {

    private Socket socket;
    private boolean abiertos;
    public PrintWriter salida;
    public BufferedReader entrada;


    public GestorStreams(String direccionIP) {
        conectar(direccionIP);
    }

    private void conectar(String direccionIP) {
        try {
            socket = new Socket(direccionIP, 5555);
            entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            salida = new PrintWriter(socket.getOutputStream(), true);
            abiertos = true;
            System.out.println(socket.isConnected());
        } catch (IOException e) {
            abiertos = false;
            e.printStackTrace();
        }
    }

    public void cerrar() {
        try {
            entrada.close();
            salida.close();
            socket.close();
            abiertos = false;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean estanAbiertos() {
        return abiertos;
    }
}

package com.aurbano.app;

import com.aurbano.gui.RegistroDialog;
import com.aurbano.renderer.IgnoresCellRenderer;
import com.aurbano.renderer.UsuarioCellRenderer;
import com.aurbano.utilidades.BarraMenu;

import javax.swing.*;
import javax.swing.text.DefaultCaret;

/**
 * Created by adriu on 20/02/2016.
 */
public class Ventana {

    JPanel panel;
    JLabel LBlogo;
    JList<String> JLcontactos;
    JList<String> JLignorar;
    JTextArea TAestado;
    JScrollPane scrollConsola;
    JLabel iconoServer;
    JLabel lbNick;
    BarraMenu barraMenu;
    RegistroDialog registroDialog;

    DefaultListModel<String> modeloContactos;
    DefaultListModel<String> modeloIgnorados;
    IgnoresCellRenderer cellRendererIgnorar;
    UsuarioCellRenderer cellRendererUsuarios;


    public Ventana() {
        inicializar();

        JFrame frame = new JFrame("Ventana");
        frame.setJMenuBar(barraMenu);
        frame.setContentPane(panel);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.pack();
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    private void inicializar() {
        barraMenu = new BarraMenu();
        registroDialog = new RegistroDialog();
        modeloContactos = new DefaultListModel<>();
        modeloIgnorados = new DefaultListModel<>();
        cellRendererIgnorar = new IgnoresCellRenderer();
        cellRendererUsuarios = new UsuarioCellRenderer();

        JLcontactos.setModel(modeloContactos);
        JLignorar.setModel(modeloIgnorados);
        JLignorar.setCellRenderer(cellRendererIgnorar);
        JLcontactos.setCellRenderer(cellRendererUsuarios);

        DefaultCaret caret = (DefaultCaret)TAestado.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
    }

}

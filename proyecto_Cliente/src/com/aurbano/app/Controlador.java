package com.aurbano.app;

import com.aurbano.gui.SalaPrivado;
import com.aurbano.utilidades.Constantes;
import com.aurbano.utilidades.GestorStreams;
import com.aurbano.utilidades.Utils;

import javax.swing.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by adriu on 20/02/2016.
 */
public class Controlador extends MouseAdapter implements ActionListener, WindowListener {

    private Ventana ventana;
    private String nick;
    private String direccionIP;
    private GestorStreams streams;
    private ArrayList<String> ignorados;
    private Map<String, SalaPrivado> privadosAbiertos;

    private boolean modoRegistro;

    public Controlador(Ventana ventana) {
        this.ventana = ventana;
        this.ignorados = new ArrayList<>();
        this.privadosAbiertos = new HashMap<>();

        File carpetaLogs = new File(Constantes.PATH_LOGS);
        File propiedades = new File(Constantes.PATH_PROPS);

        if (!carpetaLogs.exists())
            carpetaLogs.mkdir();

        if (!propiedades.exists())
            Utils.crearFicheroPropiedades();

        direccionIP = Utils.getProperty("ip");
        setListeners();
    }


    private void setListeners() {
        ventana.barraMenu.setListeners(this);
        ventana.JLcontactos.addMouseListener(this);
        ventana.JLignorar.addMouseListener(this);
    }

    private void monitorizarEstadoServidor() {
        Runnable tarea = tareaEstadoServidor();
        Executors.newScheduledThreadPool(1).scheduleAtFixedRate(tarea, 1, 5, TimeUnit.SECONDS);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        AbstractButton fuente = (AbstractButton) e.getSource();
        String actionCommand = fuente.getActionCommand();

        if (fuente instanceof JMenuItem)
            menuItemsActionPerformed(actionCommand);

        if (fuente instanceof JButton) {
            String destinatario = fuente.getActionCommand();
            SalaPrivado salaActual = privadosAbiertos.get(destinatario);
            String mensaje = "@" + destinatario + "@" + nick + "@" + salaActual.getMensaje();
            salaActual.actualizarEntrada("[ "+ nick +" ] " + salaActual.getMensaje());
            salaActual.limpiarTextField();
            streams.salida.println(mensaje);
        }
    }

    private void menuItemsActionPerformed(String actionCommand) {

        switch (actionCommand) {
            case "conectar":
                conectar_ActionPerformed();
                break;
            case "desconectar":
                desconectar_ActionPerformed();
                break;
            case "registrarse":
                registrarse_ActionPerformed();
                break;
            case "host":
                direccionServidor_ActionPerformed();
                break;
            case "monitor":
                monitorizarEstadoServidor();
                break;
            case "si":
                Utils.setLogs(true);
                break;
            case "no":
                Utils.setLogs(false);
                break;
            case "salir":
                desconectar_ActionPerformed();
                System.exit(0);
            default:
                System.out.println("no deberia llegar aqui");
        }
    }

    private void conectar_ActionPerformed() {
        ventana.registroDialog.setVisible(true);
        nick = ventana.registroDialog.getUsuario();

        if (ventana.registroDialog.isCancelado())
            return;

        conexionServidor();
    }

    private void registrarse_ActionPerformed() {
        modoRegistro = true;
        conectar_ActionPerformed();

        modoRegistro = false;
    }

    private void desconectar_ActionPerformed() {
        if (streams == null)
            return;

        streams.salida.println("/nuevaDesconexion");
        ventana.modeloContactos.clear();
        streams.cerrar();
        ventana.lbNick.setText("NICK");
    }

    private void direccionServidor_ActionPerformed() {
        direccionIP = Utils.mostrarMensajeConInput("Introduce la direccion IP");
        Utils.setProperty("ip", direccionIP);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        JList<String> fuente = (JList<String>) e.getSource();

        if (fuente == ventana.JLcontactos)
            abrirChatPrivado(e);

        if (fuente == ventana.JLignorar)
            ignorarUsusario();
    }

    private void abrirChatPrivado(MouseEvent e) {
        String destinatario = ventana.JLcontactos.getSelectedValue();
        if (destinatario == null)
            return;

        if (e.getClickCount() == 2) {
            SalaPrivado privado = new SalaPrivado(this);
            privado.setParticipantes(nick, destinatario);
            privadosAbiertos.put(destinatario, privado);
        }
    }

    private void ignorarUsusario() {
        int indice = ventana.JLignorar.getSelectedIndex();
        if (indice == -1)
            return;

        String usuario = ventana.modeloContactos.get(indice);
        for (String baneado : ignorados) {
            if (usuario.equalsIgnoreCase(baneado)) {
                admitirUsuario(usuario);
                return;
            }
        }

        if (Utils.mostrarMensajeConfirmacion("Deseas bloquear a " + usuario + " ?", "Desadmitir usuario") == 1)
            return;

        ignorados.add(usuario);
        streams.salida.println("@El usuario " + nick + " ha desadmitido a "+ usuario);
    }

    private void admitirUsuario(String usuario) {
        if (Utils.mostrarMensajeConfirmacion("Deseas Admitir a" + usuario + "?", "Readmitir usuario") == 1)
            return;

        ignorados.remove(usuario);
        streams.salida.println("@El usuario " + nick + " ha readmitido a " + usuario);
    }

    private void conexionServidor() {
        streams = new GestorStreams(direccionIP);
        ventana.lbNick.setText(nick);
        prepararWorker();
    }

    private void prepararWorker() {
        final SwingWorker<Integer, Integer> worker = new SwingWorker<Integer, Integer>() {
            @Override
            protected Integer doInBackground() throws Exception {
                boolean conexionFallida = false;
                String contrasena = ventana.registroDialog.getContrasena();

                if (modoRegistro) {
                    streams.salida.println("[registro]:" + nick);
                    streams.salida.println("[registro]:" + contrasena);
                } else {
                    streams.salida.println(nick);
                    streams.salida.println(contrasena);
                }


                while (!isCancelled()) {
                    String mensaje = streams.entrada.readLine();

                    /**
                     * Filtros de conexion
                     */
                    if (mensaje.startsWith("[ipRestringida]:")) {
                        Utils.mostrarMensajeError("Ya hay alquien conectado con tu IP", "IP ocupada");
                        conexionFallida = true;
                    }

                    if (mensaje.startsWith("[loginFallido]:")) {
                        Utils.mostrarMensajeError(mensaje.replace("[loginFallido]:", ""), "Login incorrecto");
                       conexionFallida = true;
                    }

                    if (mensaje.startsWith("[registro]:")) {
                        Utils.mostrarMensajeError(mensaje.replace("[registro]:", ""), "Registro incorrecto");
                        conexionFallida = true;
                    }

                    if (conexionFallida) {
                        desconectar_ActionPerformed();
                        cancel(true);
                    }

                    /**
                     * Actualizaciones consola
                     */
                    if (mensaje.startsWith("[info]: ")) {
                        notificarDesconexion(mensaje);

                        streams.salida.println("/conectados");
                        ventana.modeloContactos.clear();
                        ventana.modeloIgnorados.clear();
                        for (String usuario : Utils.formatearLista(streams.entrada.readLine())) {
                            if (usuario.equalsIgnoreCase(nick))
                                continue;

                            ventana.modeloContactos.addElement(usuario);
                            ventana.modeloIgnorados.addElement("");
                        }
                    }
                    /**
                     * Recibir Mensajes privados
                     */
                    if (mensaje.startsWith("@" + nick + "@")) {
                        String comandos[] = mensaje.split("@");
                        String origen = comandos[2];

                        if (usuarioIgnorado(origen))
                            continue;

                        if (!privadosAbiertos.containsKey(origen)) {
                            SalaPrivado sala = new SalaPrivado(Controlador.this);
                            sala.setParticipantes(nick, origen);
                            privadosAbiertos.put(origen, sala);
                        }

                        privadosAbiertos.get(origen).actualizarEntrada("[ "+ origen +" ] " + comandos[3]);
                    }
                }
                return null;
            }
        };
        worker.execute();
    }

    private void notificarDesconexion(String mensaje) {
        ventana.TAestado.append("\n" + mensaje);

        String usuario = mensaje.replace("[info]: ", "");
        usuario = usuario.replace(" se ha desconectado", "");
        usuario = usuario.trim();

        if (privadosAbiertos.containsKey(usuario))
            privadosAbiertos.get(usuario).actualizarEntrada("[ " + usuario + " ]" + " se ha desconectado");
    }

    public Runnable tareaEstadoServidor() {
        Runnable tarea = new Runnable() {
            @Override
            public void run() {
                try (Socket socket = new Socket(direccionIP, 5555)) {
                    ventana.iconoServer.setIcon(new ImageIcon("res/serverOn.png"));
                    new PrintWriter(socket.getOutputStream(), true).println("/debug");
                } catch (IOException ex) {
                    ventana.iconoServer.setIcon(new ImageIcon("res/off.png"));
                }
            }
        };

        return tarea;
    }

    public boolean usuarioIgnorado(String usuario) {
        for (String baneado : ignorados)
            if (usuario.equalsIgnoreCase(baneado))
               return true;

        return false;
    }

    @Override
    public void windowClosing(WindowEvent e) {
        String usuario = ((JFrame) e.getSource()).getTitle();

        if (Utils.logsActivos()) {
            SalaPrivado privado = privadosAbiertos.get(usuario);
            Utils.guardarLog(usuario, privado.getConversacion());
        }

        privadosAbiertos.remove(usuario);
    }

    @Override
    public void windowClosed(WindowEvent e) {}
    @Override
    public void windowOpened(WindowEvent e) {}
    @Override
    public void windowIconified(WindowEvent e) {}
    @Override
    public void windowDeiconified(WindowEvent e) {}
    @Override
    public void windowActivated(WindowEvent e) {}
    @Override
    public void windowDeactivated(WindowEvent e) {}
}
